# Manjaro Forum Dark Theme

A dark theme for the official Manjaro Linux Forum.

**Note**: Manjaro Forum also has its own built-in dark theme. To enable it, log in, go to your account preferences, select "Interface" on the left, and choose a theme to your liking.

This style has been written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on one of the banners below and a new window of Stylus should open, asking you to confirm to install the style.

**Development version** (updated after every commit, reinstall it manually to see the latest changes): [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/manjaro-forum-dark-theme/raw/master/manjaro-forum-dark-theme.user.css)  
**Stable version** (updated less frequently, with version number increase): coming soon™

## Mirrors

The style can also be installed from the following websites (although these versions may be slightly out of date):

[OpenUserCSS](https://openusercss.org/) (coming soon™)  
[Userstyles.org](https://userstyles.org/styles/128016/manjaro-forums-dark-theme-discourse)

## Screenshots

There's a rather old screenshot on [userstyles.org](https://userstyles.org/styles/128016/manjaro-forums-dark-theme-discourse). I'll add updated ones here once I've fixed a few bugs.

## Submitting issues

Before you open a new issue ticket, please, make sure you’ve done the following:

- Install the latest development version. (Open the style from [here](https://gitlab.com/maxigaz/gitlab-dark/raw/master/gitlab-dark.user.css). If you have installed it earlier, tell Stylus to reinstall it, overriding the version you currently have.)
- If you’re logged in to the forum, make sure you have “Manjaro” selected as a theme in Preferences → Interface.

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.
